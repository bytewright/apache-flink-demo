package net.bytewright.flinkml.climate

import org.apache.flink.api.java.ExecutionEnvironment
import org.apache.flink.ml.common.LabeledVector
import org.apache.flink.ml.math.DenseVector
import org.apache.flink.ml.regression.MultipleLinearRegression

object FlinkMlExample {
  def linearRegression(data: Array[Array[Double]]): Int = {
    //val env = ExecutionEnvironment.createLocalEnvironment()
    // Create multiple linear regression learner
    /*val mlr = MultipleLinearRegression()
      .setIterations(10)
      .setStepsize(0.5)
      .setConvergenceThreshold(0.001)
    val list = data
      .map(array ⇒ Tuple2(array(0), array(1)))
      .map(tuple ⇒ LabeledVector(tuple(0), DenseVector(1, tuple(1))))
      .iterator
*/
    // Obtain training and testing data set
    //    val trainingDS: DataSet[LabeledVector] = env.fromCollection(list, TypeInformation[LabeledVector])
    //        val testingDS: DataSet[Vector] = ...

    // Fit the linear model to the provided data
    //    mlr.fit(trainingDS)

    // Calculate the predictions for the test data
    //    val predictions = mlr.predict(testingDS)
    data.length
  }
}
