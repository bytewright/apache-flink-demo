package net.bytewright.flinkdemo;

import java.util.UUID;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

import de.javakaffee.kryoserializers.UUIDSerializer;
import de.javakaffee.kryoserializers.guava.ImmutableListSerializer;
import net.bytewright.flinkdemo.climate.ClimateData;
import net.bytewright.flinkdemo.climate.ClimateStreamer;
import net.bytewright.flinkdemo.util.DataSetProvider;

public class App {
  private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

  public static void main(String[] args) {
    ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
    configureEnvironment(env);
    DataSetProvider dataSetProvider = new DataSetProvider(env);
    //    new StringStreamer(dataSetProvider, env).run();
    new ClimateStreamer(dataSetProvider, env).run();
  }

  public static void configureEnvironment(ExecutionEnvironment env) {
    LOGGER.info("Configuring flink for local exec");
    env.setParallelism(2); // for local
    ExecutionConfig envConfig = env.getConfig();
    envConfig.registerTypeWithKryoSerializer(UUID.class, UUIDSerializer.class);
    envConfig.registerTypeWithKryoSerializer(ImmutableList.class, ImmutableListSerializer.class);
    envConfig.registerTypeWithKryoSerializer(ImmutableList.of().getClass(), ImmutableListSerializer.class);
    envConfig.registerTypeWithKryoSerializer(ImmutableList.of(1).getClass(), ImmutableListSerializer.class);
    envConfig.registerTypeWithKryoSerializer(ImmutableList.of(1, 2, 3).subList(1, 2).getClass(), ImmutableListSerializer.class);
    envConfig.registerTypeWithKryoSerializer(ImmutableList.of().reverse().getClass(), ImmutableListSerializer.class);
  }

  public static void configureStreamEnvironment(StreamExecutionEnvironment env) {
    LOGGER.info("Configuring flink stream for local exec");
    ExecutionConfig envConfig = env.getConfig();
    envConfig.enableSysoutLogging();
    env.setParallelism(2); // for local
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
    envConfig.registerPojoType(ClimateData.class);
  }
}
