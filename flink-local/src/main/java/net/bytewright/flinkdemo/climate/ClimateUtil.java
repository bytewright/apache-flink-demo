package net.bytewright.flinkdemo.climate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO: auto-generated ClimateUtil description
 */
public class ClimateUtil {
  private static final Logger LOGGER = LoggerFactory.getLogger(ClimateUtil.class);

  public static ClimateData avgTemp(ClimateData value1, ClimateData value2) {
    int days = value1.getAvgOverDays() + value2.getAvgOverDays();
    double temp = value1.getTagesmittelTemperatur() + value2.getTagesmittelTemperatur();
    ClimateData climateData = new ClimateData();
    climateData.avgOverDays = days;
    climateData.tagesmittelTemperatur = temp / (double) days;
    climateData.recordingDate = value1.recordingDate;
    return climateData;
  }
}
