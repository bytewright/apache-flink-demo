package net.bytewright.flinkdemo.climate;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClimateData implements Serializable {
  private static final Logger LOGGER = LoggerFactory.getLogger(ClimateData.class);

  public static final ClimateData EMPTY = new ClimateData();
  int avgOverDays;
  LocalDate recordingDate;
  String qn3;
  String maximaleWindspitze; //fx
  String tagesmittelWindgeschwindigkeit;//fm
  String qn4;
  double niederschlagshoehe;//rsk
  int niederschlagsform;//rskf
  double sonnenscheindauerTagessumme;//sdk
  int schneehoeheTageswert;//shkTag
  double tagesmittelBedeckungsgrad;//nm
  double tagesmittelDampfdruck;//vpm
  double tagesmittelLuftdruck;//pm
  double tagesmittelTemperatur;//tmk
  double tagesmittelRelativeFeuchte;//upm
  double tagesmaximumLufttemperatur;//txk
  double tagesminimumLufttemperatur;//tnk
  double minLufttemperaturBoden;//tkg

  ClimateData() {
  }

  private ClimateData(List<String> strings) throws NumberFormatException, NullPointerException {
    String date = getOrNull(strings, 1);
    recordingDate = date != null ? LocalDate.parse(date, DateTimeFormatter.BASIC_ISO_DATE) : null;
    qn3 = getOrNull(strings, 2);
    maximaleWindspitze = getOrNull(strings, 3);
    tagesmittelWindgeschwindigkeit = getOrNull(strings, 4);
    qn4 = getOrNull(strings, 5);
    niederschlagshoehe = Double.parseDouble(getOrNull(strings, 6));
    niederschlagsform = Integer.parseInt(getOrNull(strings, 7));
    sonnenscheindauerTagessumme = Double.parseDouble(getOrNull(strings, 8));
    schneehoeheTageswert = Integer.parseInt(getOrNull(strings, 9));
    tagesmittelBedeckungsgrad = Double.parseDouble(getOrNull(strings, 10));
    tagesmittelDampfdruck = Double.parseDouble(getOrNull(strings, 11));
    tagesmittelLuftdruck = Double.parseDouble(getOrNull(strings, 12));
    tagesmittelTemperatur = Double.parseDouble(getOrNull(strings, 13));
    tagesmittelRelativeFeuchte = Double.parseDouble(getOrNull(strings, 14));
    tagesmaximumLufttemperatur = Double.parseDouble(getOrNull(strings, 15));
    tagesminimumLufttemperatur = Double.parseDouble(getOrNull(strings, 16));
    minLufttemperaturBoden = Double.parseDouble(getOrNull(strings, 17));
    avgOverDays = 1;
  }

  public int getSchneehoeheTageswert() {
    return schneehoeheTageswert;
  }

  public LocalDate getRecordingDate() {
    return recordingDate;
  }

  public String getMaximaleWindspitze() {
    return maximaleWindspitze;
  }

  public double getTagesmittelBedeckungsgrad() {
    return tagesmittelBedeckungsgrad;
  }

  public double getNiederschlagshoehe() {
    return niederschlagshoehe;
  }

  public int getNiederschlagsform() {
    return niederschlagsform;
  }

  public double getSonnenscheindauerTagessumme() {
    return sonnenscheindauerTagessumme;
  }

  public String getTagesmittelWindgeschwindigkeit() {
    return tagesmittelWindgeschwindigkeit;
  }

  public double getTagesmittelDampfdruck() {
    return tagesmittelDampfdruck;
  }

  public double getTagesmittelLuftdruck() {
    return tagesmittelLuftdruck;
  }

  public double getTagesmittelTemperatur() {
    return tagesmittelTemperatur;
  }

  public double getTagesmittelRelativeFeuchte() {
    return tagesmittelRelativeFeuchte;
  }

  public double getTagesmaximumLufttemperatur() {
    return tagesmaximumLufttemperatur;
  }

  public double getTagesminimumLufttemperatur() {
    return tagesminimumLufttemperatur;
  }

  public double getMinLufttemperaturBoden() {
    return minLufttemperaturBoden;
  }

  public String getQn3() {
    return qn3;
  }

  public String getQn4() {
    return qn4;
  }

  public int getAvgOverDays() {
    return avgOverDays;
  }

  private String getOrNull(List<String> strings, int i) {
    return StringUtils.stripToNull(strings.get(i));
  }

  public static ClimateData from(String value) {
    List<String> strings = Arrays.stream(value.split(";"))
        .map(String::trim)
        .collect(Collectors.toList());
    if (strings.size() == 19 && strings.get(0).equals("1270")) {
      try {
        return new ClimateData(strings);
      } catch (NumberFormatException | NullPointerException e) {
        //        throw e;
        //        LOGGER.info("Error while parsing {}", value, e);
      }
    }
    return EMPTY;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(getRecordingDate())
        .append(getQn3())
        .append(maximaleWindspitze)
        .append(tagesmittelWindgeschwindigkeit)
        .append(getQn4())
        .append(niederschlagshoehe)
        .append(niederschlagsform)
        .append(sonnenscheindauerTagessumme)
        .append(schneehoeheTageswert)
        .append(tagesmittelBedeckungsgrad)
        .append(tagesmittelDampfdruck)
        .append(tagesmittelLuftdruck)
        .append(tagesmittelTemperatur)
        .append(tagesmittelRelativeFeuchte)
        .append(tagesmaximumLufttemperatur)
        .append(tagesminimumLufttemperatur)
        .append(minLufttemperaturBoden)
        .append(getAvgOverDays())
        .toHashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;

    if (o == null || getClass() != o.getClass())
      return false;

    ClimateData that = (ClimateData) o;

    return new EqualsBuilder()
        .append(getAvgOverDays(), that.getAvgOverDays())
        .append(getRecordingDate(), that.getRecordingDate())
        .append(getQn3(), that.getQn3())
        .append(maximaleWindspitze, that.maximaleWindspitze)
        .append(tagesmittelWindgeschwindigkeit, that.tagesmittelWindgeschwindigkeit)
        .append(getQn4(), that.getQn4())
        .append(niederschlagshoehe, that.niederschlagshoehe)
        .append(niederschlagsform, that.niederschlagsform)
        .append(sonnenscheindauerTagessumme, that.sonnenscheindauerTagessumme)
        .append(schneehoeheTageswert, that.schneehoeheTageswert)
        .append(tagesmittelBedeckungsgrad, that.tagesmittelBedeckungsgrad)
        .append(tagesmittelDampfdruck, that.tagesmittelDampfdruck)
        .append(tagesmittelLuftdruck, that.tagesmittelLuftdruck)
        .append(tagesmittelTemperatur, that.tagesmittelTemperatur)
        .append(tagesmittelRelativeFeuchte, that.tagesmittelRelativeFeuchte)
        .append(tagesmaximumLufttemperatur, that.tagesmaximumLufttemperatur)
        .append(tagesminimumLufttemperatur, that.tagesminimumLufttemperatur)
        .append(minLufttemperaturBoden, that.minLufttemperaturBoden)
        .isEquals();
  }

  @Override
  public String toString() {
    return "ClimateData{" +
        "avgOverDays=" + avgOverDays +
        ", recordingDate=" + recordingDate +
        ", qn3='" + qn3 + '\'' +
        ", maximaleWindspitze='" + maximaleWindspitze + '\'' +
        ", tagesmittelWindgeschwindigkeit='" + tagesmittelWindgeschwindigkeit + '\'' +
        ", qn4='" + qn4 + '\'' +
        ", niederschlagshoehe=" + niederschlagshoehe +
        ", niederschlagsform=" + niederschlagsform +
        ", sonnenscheindauerTagessumme=" + sonnenscheindauerTagessumme +
        ", schneehoeheTageswert=" + schneehoeheTageswert +
        ", tagesmittelBedeckungsgrad=" + tagesmittelBedeckungsgrad +
        ", tagesmittelDampfdruck=" + tagesmittelDampfdruck +
        ", tagesmittelLuftdruck=" + tagesmittelLuftdruck +
        ", tagesmittelTemperatur=" + tagesmittelTemperatur +
        ", tagesmittelRelativeFeuchte=" + tagesmittelRelativeFeuchte +
        ", tagesmaximumLufttemperatur=" + tagesmaximumLufttemperatur +
        ", tagesminimumLufttemperatur=" + tagesminimumLufttemperatur +
        ", minLufttemperaturBoden=" + minLufttemperaturBoden +
        '}';
  }
}
