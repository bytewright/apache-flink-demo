package net.bytewright.flinkdemo.climate;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Streams;
import net.bytewright.flinkdemo.App;
import net.bytewright.flinkdemo.util.DataSetProvider;
import net.bytewright.flinkml.climate.FlinkMlExample;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamUtils;
import org.apache.flink.streaming.api.environment.LocalStreamEnvironment;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.IsoFields;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("UnstableApiUsage")
public class ClimateStreamer implements Runnable {
  private static final Logger LOGGER = LoggerFactory.getLogger(ClimateStreamer.class);
  private final DataSetProvider dataSetProvider;
  private final ExecutionEnvironment env;

  public ClimateStreamer(DataSetProvider dataSetProvider, ExecutionEnvironment env) {
    this.dataSetProvider = dataSetProvider;
    this.env = env;
  }

  @Override
  public void run() {
    try {
      runWeeklyAvgExample();
      List<Tuple2<Double, Double>> weeklyAvgExample = ImmutableList.of();
      runMLExample(weeklyAvgExample);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void runWeeklyAvgExample() throws Exception {
    StreamExecutionEnvironment env = LocalStreamEnvironment.getExecutionEnvironment();
    App.configureStreamEnvironment(env);
    DataStream<Tuple5<String, String, String, String, Double>> streamOperator;
    streamOperator = dataSetProvider
      .getClimateData(env)
      .map(tupleMapper())
      .assignTimestampsAndWatermarks(timeStampExtractor())
      .timeWindowAll(Time.days(7))
      .aggregate(toBatch())
      .process(processWeekBatch())
      .map(outputMapper());

    streamOperator.writeAsCsv("out-weekly-temp", FileSystem.WriteMode.OVERWRITE, "\n", ";");

    LOGGER.info("{}", env.execute());

    Iterator<Tuple5<String, String, String, String, Double>> iterator;
    iterator = DataStreamUtils.collect(streamOperator);

    Streams.stream(iterator)
      .sorted(Comparator.comparing(row -> row.f0))
      .collect(Collectors.toList())
      .forEach(this::printResultRow);
  }

  private void printResultRow(Tuple5<String, String, String, String, Double> t) {
    LOGGER.info("{}, {}, KW {}, {} Tage, {} C°",
      t.f0, t.f1, t.f2, t.f3, String.format("%.4f", t.f4));
  }

  private void runMLExample(List<Tuple2<Double, Double>> weeklyAvgExample) {
    LOGGER.info("running ml example with {} datapoints", weeklyAvgExample.size());
    double[][] data = new double[weeklyAvgExample.size()][2];
    for (int i = 0; i < weeklyAvgExample.size(); i++) {
      Tuple2<Double, Double> tuple2 = weeklyAvgExample.get(i);
      data[i][0] = tuple2.f0;
      data[i][1] = tuple2.f1;
    }
    int i = FlinkMlExample.linearRegression(data);
    LOGGER.info("test {}", i);
  }

  private MapFunction<Tuple3<ZonedDateTime, ZonedDateTime, Double>, Tuple5<String, String, String, String, Double>> outputMapper() {
    return new MapFunction<Tuple3<ZonedDateTime, ZonedDateTime, Double>, Tuple5<String, String, String, String, Double>>() {
      @Override
      public Tuple5<String, String, String, String, Double> map(Tuple3<ZonedDateTime, ZonedDateTime, Double> tuple3)
        throws Exception {
        return Tuple5.of(
          tuple3.f0.format(DateTimeFormatter.ISO_LOCAL_DATE), tuple3.f1.format(DateTimeFormatter.ISO_LOCAL_DATE),
          String.format("%2d", tuple3.f0.get(IsoFields.WEEK_OF_WEEK_BASED_YEAR)),
          String.valueOf(Period.between(tuple3.f0.toLocalDate(), tuple3.f1.toLocalDate()).getDays() + 1),
          tuple3.f2);
      }
    };
  }

  private ProcessFunction<List<Tuple3<ZonedDateTime, Integer, Double>>, Tuple3<ZonedDateTime, ZonedDateTime, Double>> processWeekBatch() {
    return new ProcessFunction<List<Tuple3<ZonedDateTime, Integer, Double>>, Tuple3<ZonedDateTime, ZonedDateTime, Double>>() {
      @Override
      public void processElement(List<Tuple3<ZonedDateTime, Integer, Double>> value, Context ctx,
                                 Collector<Tuple3<ZonedDateTime, ZonedDateTime, Double>> out) throws Exception {
        ZonedDateTime min = value.stream().min(Comparator.comparing(o -> o.f0)).map(t -> t.f0).orElse(null);
        ZonedDateTime max = value.stream().max(Comparator.comparing(o -> o.f0)).map(t -> t.f0).orElse(null);
        double sumTemp = value.stream().mapToDouble(tuple -> tuple.f2).sum();
        out.collect(Tuple3.of(min, max, sumTemp / (double) value.size()));
      }
    };

  }

  private AggregateFunction<Tuple3<ZonedDateTime, Integer, Double>, List<Tuple3<ZonedDateTime, Integer, Double>>, List<Tuple3<ZonedDateTime, Integer, Double>>> toBatch() {
    return new AggregateFunction<Tuple3<ZonedDateTime, Integer, Double>, List<Tuple3<ZonedDateTime, Integer, Double>>, List<Tuple3<ZonedDateTime, Integer, Double>>>() {
      @Override
      public List<Tuple3<ZonedDateTime, Integer, Double>> createAccumulator() {
        return new ArrayList<>();
      }

      @Override
      public List<Tuple3<ZonedDateTime, Integer, Double>> add(Tuple3<ZonedDateTime, Integer, Double> value,
                                                              List<Tuple3<ZonedDateTime, Integer, Double>> accumulator) {
        accumulator.add(value);
        return accumulator;
      }

      @Override
      public List<Tuple3<ZonedDateTime, Integer, Double>> getResult(List<Tuple3<ZonedDateTime, Integer, Double>> accumulator) {
        return accumulator;
      }

      @Override
      public List<Tuple3<ZonedDateTime, Integer, Double>> merge(List<Tuple3<ZonedDateTime, Integer, Double>> a,
                                                                List<Tuple3<ZonedDateTime, Integer, Double>> b) {
        a.addAll(b);
        return a;
      }
    };
  }

  private MapFunction<ClimateData, Tuple3<ZonedDateTime, Integer, Double>> tupleMapper() {
    return new MapFunction<ClimateData, Tuple3<ZonedDateTime, Integer, Double>>() {
      @Override
      public Tuple3<ZonedDateTime, Integer, Double> map(ClimateData value) throws Exception {
        return Tuple3.of(value.getRecordingDate().atStartOfDay(ZoneId.of("Europe/Berlin")),
          value.avgOverDays,
          value.getTagesmittelTemperatur());
      }
    };
  }

  private AssignerWithPeriodicWatermarks<Tuple3<ZonedDateTime, Integer, Double>> timeStampExtractor() {
    return new AssignerWithPeriodicWatermarks<Tuple3<ZonedDateTime, Integer, Double>>() {
      long epochMilli = 0;

      @Override
      public long extractTimestamp(Tuple3<ZonedDateTime, Integer, Double> element, long previousElementTimestamp) {
        long epochMilli = element.f0.toInstant().toEpochMilli();
        this.epochMilli = epochMilli;
        return epochMilli;
      }

      @Nullable
      @Override
      public Watermark getCurrentWatermark() {
        return new Watermark(epochMilli);
      }
    };
  }
}
