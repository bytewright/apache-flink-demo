package net.bytewright.flinkdemo.strings;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.bytewright.flinkdemo.util.DataSetProvider;

/**
 * TODO: auto-generated StringStreamer description
 */
public class StringStreamer implements Runnable {
  private static final Logger LOGGER = LoggerFactory.getLogger(StringStreamer.class);
  private final DataSetProvider dataSetProvider;
  private final ExecutionEnvironment env;

  public StringStreamer(DataSetProvider dataSetProvider, ExecutionEnvironment env) {

    this.dataSetProvider = dataSetProvider;
    this.env = env;
  }

  @Override
  public void run() {
    try {
      runNamesExample();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void runNamesExample() throws Exception {
    long count = dataSetProvider.getStrings()
        .count();
    LOGGER.info("Dataset contains {} entries", count);
  }
}
