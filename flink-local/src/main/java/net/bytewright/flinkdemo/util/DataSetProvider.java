package net.bytewright.flinkdemo.util;

import java.net.URL;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.bytewright.flinkdemo.climate.ClimateData;

/**
 * TODO: auto-generated DataSetProvider description
 */
public class DataSetProvider {
  private static final Logger LOGGER = LoggerFactory.getLogger(DataSetProvider.class);
  private final ExecutionEnvironment env;

  public DataSetProvider(ExecutionEnvironment env) {
    this.env = env;
  }

  public DataSet<String> getStrings() {
    return env.fromElements("Hallo Welt");
  }

  public DataStream<ClimateData> getClimateData(StreamExecutionEnvironment env) {
    URL resource = this.getClass().getClassLoader().getResource("klimadata.txt");
    LOGGER.info("Loading data from {}", resource);
    return env.readTextFile(resource.getPath(), "UTF-8")
        .map(ClimateData::from)
        .filter(value -> value != null && !ClimateData.EMPTY.equals(value));
  }
}
